require 'test_helper'

class TableViewerFlowTest < ActionDispatch::IntegrationTest
  test "root goes to table_viewers#new" do
    get "/"
    assert_select "p", "Go ahead and see your first table!"
  end

  test "can create an table viewer with a username and table that exist" do
    get "/table_viewer/new"
    assert_response :success

    post "/table_viewer",
      table_viewer: { name: "rambo-test", table: "mnmappluto", page: 0}
    assert_response :success
    assert_select "table"
  end

  test "nav_links work" do
    get "/table_viewer/new"
    assert_response :success

    post "/table_viewer",
      table_viewer: { name: "rambo-test", table: "mnmappluto", page: 0}
    assert_response :success
    assert_select "#table_viewer_page", { value: "0" }

    post "/table_viewer",
      table_viewer: { name: "rambo-test", table: "mnmappluto", page: 0, page_inc: 1}
    assert_response :success
    assert_select "#table_viewer_page", { value: "1" }

    post "/table_viewer",
      table_viewer: { name: "rambo-test", table: "mnmappluto", page: 0, page_inc: -1}
    assert_response :success
    assert_select "#table_viewer_page", { value: "0" }
  end

  test "can't create a table viewer with a username that doesn't exist" do
    get "/table_viewer/new"
    assert_response :success

    post "/table_viewer",
      table_viewer: { name: "fff ", table: "mnmappluto", page: 0}
    assert_response :success
    assert_select "div", "Please make sure the user/table combo are available in carto :)"
  end

  test "can't create a table viewer with a table that doesn't exist" do
    get "/table_viewer/new"
    assert_response :success

    post "/table_viewer",
      table_viewer: { name: "rambo-test ", table: "fff", page: 0}
    assert_response :success
    assert_select "div", "Please make sure the user/table combo are available in carto :)"
  end
end
