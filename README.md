# Carto Table Viewer

* I chose to do this exercise since I clearly saw the solution in my head when reading the statement and thought it was the path to go given the time limitation.

* Ruby version: 2.3.3, there's a .ruby-version file and a .ruby-gemset file for using rvm (https://rvm.io/rvm/install).

* Run tests with `bundle exec ruby -Itest test/integration/table_viewer_flow_test.rb`

## The basics

* A simple restful controller with a '#new' action which is basically blank, just serves its purpose as pathway to start using the app, a '#create' action that handles the request and a '#show' action that renders the result.

* A model with just a few methods to access the different parts of the object that results from the request.

* A new and a show view to render the form, and results. The form sits on the layout since it is a permanent element.

* A very basic integration test with different assertions(I chose this strategy since it is the fastest way to cover most of the stack)

* Styles are powered by thoughtbot's css stack (bourbon, neat, bitters, and refills). They basically provide mixins, and predefined styles and variables for styling in a rush. I used normalize as my reset css.

## Other considerations (things to improve)

* I strongly oppose testing an external api itself, and favor stubbing the response.

* I am not performing any sanitation of the user input, since I am not directly working with any database. Though I understand it is a much desired practice, in this case i take it for granted this is being done on Carto's side.

* Every user action on the app fires a request to the cartosql api, I am aware there are many caching strategies I could implement to improve request times and overall ui experience.

* As I mentioned before, I use a simple integration test with many assertions. Given time, I would have followed a different approach (unit + integration tests, unit tests for each public method and integration tests for each feature/use case)

* I use a simple rescue for controlling errors when the api call response does not return any valid json (when the user doesn't exist, when unpermitted characters are used etc), i realize this is not a best practice, and would definitely be one of the things i would change.

* I realize that separating the view into components that don't always change and providing a url that allows the user to use the browsers buttons are basic features that should be added if this app were to be used, and are features that can be implemented with backbone or even rails' own turbolinks.

* I would include js in modules to keep it namespaced and avoid collisions.

* Since I am not using active records I limited the frameworks that will be loaded in config/application.rb, and would most definitely remove all references to activerecords as well as sqlite to keep it clean

* I'm also not controlling if a user manipulates the 'previous' button from the browser, I realize it is an aspect to improve.
