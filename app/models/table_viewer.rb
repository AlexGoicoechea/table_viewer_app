class TableViewer

  attr_accessor :name, :table, :page

  def initialize(args)
    @page_inc = args[:page_inc] || 0
    @page = Integer(args[:page].empty? ? 0 : args[:page]) + Integer(@page_inc)
    @name = args[:name]
    @table = args[:table]
    url = "https://#{@name}.carto.com:443/api/v2/sql?q=select * from public.#{@table} LIMIT 25 OFFSET #{25 * @page + 1}"
    @response = TableViewer.fetch_json(url)
  end

  def fields
    @response["fields"].keys
  end

  def results
    @response["rows"]
  end

  def valid?
    @response.present? && @response["rows"].present?
  end

  private

  def self.fetch_json(url)
    uri = URI(url)
    response = Net::HTTP.get(uri)
    JSON.parse(response)
  rescue
    ''
  end
end
