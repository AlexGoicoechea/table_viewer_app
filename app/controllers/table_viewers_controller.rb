class TableViewersController < ApplicationController
  before_action :set_table_viewer, only: %w(create)

  def new
  end

  def create
    flash[:error] = "Please make sure the user/table combo are available in carto :)" unless @table_viewer.valid?
    render 'show'
  end

  def show
  end

  private

  def table_viewer_params
    params.require(:table_viewer).permit(:name, :table, :page, :page_inc)
  end

  def set_table_viewer
    @table_viewer = TableViewer.new(table_viewer_params)
  end
end
