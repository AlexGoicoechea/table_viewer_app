Rails.application.routes.draw do
  root to: 'table_viewers#new'
  get '/table_viewer', to: 'table_viewers#new'
  resource :table_viewer, only: %w(create new show)
end
